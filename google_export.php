<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$file = fopen($_SERVER['DOCUMENT_ROOT'] . '/google_export.xml', 'w');
$xml .= '<?xml version="1.0" encoding="UTF-8"?>' .
    '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">' .
    '<channel>';


//Получаем инф. по сайту
$site_id = SITE_ID;
if (empty($site_id) || $site_id == "ru")
    $site_id = 's1';

$rsSites = CSite::GetByID($site_id);
$arSite = $rsSites->Fetch();
$xml .= "<title>$arSite[SITE_NAME]</title>";
$xml .= "<link>http://$arSite[SERVER_NAME]</link>";
$srv_name=$arSite["SERVER_NAME"];

//Получаем список каталога товаров
$arOrder = Array(
    'ID' => 'ASC'
);
$arFilter = Array(
    'IBLOCK_ID' => 2,
    'ACTIVE' => 'Y'
);
$arSelect = Array(
    'NAME',
    'ID',
    'IBLOCK_SECTION_ID',
    'CATALOG_GROUP_1',
    'CODE',
    'DETAIL_PICTURE'
);
$dbItems = CIBlockElement::GetList($arOrder, $arFilter, false, false, null);
while ($arItem = $dbItems->Fetch()) {
    //Детальная инф. о товаре
    $arElement = CIblockElement::GetById($arItem["ID"])->GetNext();

    //Изображение
    $img = CFile::GetFileArray($arElement["DETAIL_PICTURE"]);
    $img = "http://$srv_name$img[SRC]";


    //Получаем список торговых предложений товара
    //Цены в торговых предложениях на товар одинаковы, по этому берем последнюю.
    $IBLOCK_ID = 2;
    $ID = $arItem["ID"];
    $arInfo = CCatalogSKU::GetInfoByProductIBlock($IBLOCK_ID);
    if (is_array($arInfo)) {
        $rsOffers = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $arInfo['IBLOCK_ID'], 'PROPERTY_' . $arInfo['SKU_PROPERTY_ID'] => $ID));
        while ($arOffer = $rsOffers->GetNext()) {

            $db_res = CPrice::GetList(
                array(),
                array(
                    "PRODUCT_ID" => $arOffer["ID"],
                    "CATALOG_GROUP_ID" => "1"
                )
            );

            if ($ar_res = $db_res->Fetch()) {

                $arElement["PRICE"] = round($ar_res['PRICE']);
                $arElement["CURRENCY"] = $ar_res['CURRENCY'];
            }
        }

    }

    //проверяем скидки
    $arDiscounts = CCatalogDiscount::GetDiscountByProduct($ID);
    if (is_array($arDiscounts) && sizeof($arDiscounts) > 0) {
        $final_price = round(CCatalogProduct::CountPriceWithDiscount($arElement["PRICE"], $arElement["CURRENCY"], $arDiscounts));

    }
    //Чистим описание
    $desc = strip_tags($arElement['DETAIL_TEXT']);
    $desc = str_replace('  ', ' ', $desc);
    $desc = str_replace('&ndash;', '-', $desc);
    $desc = str_replace("\t\t\t\t\t\t\t\n", "\n", $desc);
    $desc = str_replace("\n\n", "\n", $desc);

    $xml .= "<item>\r\n<g:id>$arItem[ID]</g:id>\r\n" .
        "<title>$arItem[NAME]</title>\r\n" .
        "<link>http://$srv_name$arElement[DETAIL_PAGE_URL]</link>\r\n" .
        "<description>$desc</description>\r\n" .
        "<g:image_link>$img</g:image_link>\r\n" .
        "<g:price>$arElement[PRICE]\t$arElement[CURRENCY]</g:price>\r\n";
    if ($final_price)
        $xml .= "<g:sale_price>$final_price\t$arElement[CURRENCY]</g:sale_price>\r\n";

    $xml .= "<g:condition>new</g:condition>\r\n" .
        "<g:availability>in stock</g:availability>\r\n" .
        "</item>\r\n";
    unset($img);
    unset($final_price);
}

$xml .= '</channel></rss>';

fwrite($file, $xml);
fclose($file);